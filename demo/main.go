package main

import (
	"fmt"

	"gitlab.com/zecchan/cage/io"
)

func main() {
	fmt.Println("Hello World")

	ms := new(io.MemoryStream)
	ms.Init([]byte{1, 2, 3, 4, 5})
	ms.Write([]byte{6, 7, 8}, 0, 3)
	ms.Seek(-3, io.SeekOrigins.End)
	ms.Write([]byte{9, 10, 11, 12, 13, 14}, 0, 6)
	ms.SetLength(20)
	ms.Write([]byte{15, 16, 17}, 0, 3)
	fmt.Println(ms.MemoryBuffer)
}
