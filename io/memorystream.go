package io

import "errors"

// MemoryStream hold a sequence of bytes in memory
type MemoryStream struct {
	MemoryBuffer []byte
	position     int
}

// Init will initialize the memory stream
func (ms *MemoryStream) Init(buffer []byte) {
	ms.MemoryBuffer = buffer
	if ms.MemoryBuffer == nil {
		ms.MemoryBuffer = []byte{}
	}
	ms.position = ms.Length()
}

// CanRead gets a value indicating whether the current stream supports writing.
func (ms *MemoryStream) CanRead() bool {
	return true
}

// CanWrite gets a value indicating whether the current stream supports writing.
func (ms *MemoryStream) CanWrite() bool {
	return true
}

// CanSeek gets a value indicating whether the current stream supports seeking.
func (ms *MemoryStream) CanSeek() bool {
	return true
}

// EOF gets a value indicating whether the current stream supports seeking.
func (ms *MemoryStream) EOF() bool {
	return ms.position >= ms.Length()
}

// Length gets the length in bytes of the stream.
func (ms *MemoryStream) Length() int {
	return len(ms.MemoryBuffer)
}

// Position gets the position within the current stream.
func (ms *MemoryStream) Position() int {
	return ms.position
}

// Close the current stream and releases any resources (such as sockets and file handles) associated with the current stream.
func (ms *MemoryStream) Close() {
	ms.Flush()
	ms.MemoryBuffer = []byte{}
	ms.position = 0
}

// Flush clears all buffers for this stream and causes any buffered data to be written to the underlying device or stream.
func (ms *MemoryStream) Flush() {
}

// CopyTo reads the bytes from the current stream and writes them to another stream. Returns the number of bytes written.
func (ms *MemoryStream) CopyTo(stream Stream) int {
	return ms.CopyBufferedTo(stream, 81920)
}

// CopyBufferedTo reads the bytes from the current stream and writes them to another stream, using a specified buffer size. Returns the number of bytes written.
func (ms *MemoryStream) CopyBufferedTo(stream Stream, bufferSize int) int {
	var written int
	remaining := ms.Length() - ms.position
	for !ms.EOF() {
		readSize := bufferSize
		if readSize > remaining {
			readSize = remaining
		}
		buffer := ms.MemoryBuffer[ms.position : ms.position+readSize]
		op := stream.Write(buffer, 0, len(buffer))
		ms.position += op
		written += op
		if op < readSize {
			return written
		}
	}
	return written
}

// Read a sequence of bytes from the current stream and advances the position within the stream by the number of bytes read. Returns the number of bytes read.
func (ms *MemoryStream) Read(buffer []byte, offset int, length int) int {
	remaining := ms.Length() - ms.position
	var readSize = length
	if readSize > remaining {
		readSize = remaining
	}
	var bufferSlice = buffer[offset : offset+length]
	var streamSlice = ms.MemoryBuffer[ms.position : ms.position+readSize]
	for i, v := range bufferSlice {
		streamSlice[i] = v
	}
	ms.position += readSize
	return readSize
}

// Seek sets the position within the current stream.
func (ms *MemoryStream) Seek(offset int, origin SeekOrigin) error {
	oPos := ms.position
	if origin == 2 {
		oPos = ms.Length()
	} else if origin == 0 {
		oPos = 0
	}
	oPos += offset
	if oPos < 0 || oPos >= ms.Length() {
		return errors.New("Index is out of bound")
	}
	ms.position = oPos
	return nil
}

// SetLength sets the length of the current stream.
func (ms *MemoryStream) SetLength(length int) {
	newBuffer := []byte{}
	copySize := length
	if length > ms.Length() {
		copySize = ms.Length()
	}
	for _, v := range ms.MemoryBuffer[0:copySize] {
		newBuffer = append(newBuffer, v)
	}
	if copySize < length {
		for i := copySize; i < length; i++ {
			newBuffer = append(newBuffer, 0)
		}
	}
	ms.MemoryBuffer = newBuffer
	if ms.position > ms.Length() {
		ms.position = ms.Length()
	}
}

// Write a sequence of bytes to the current stream and advances the position within the stream by the number of bytes written. Returns the number of bytes written.
func (ms *MemoryStream) Write(buffer []byte, offset int, length int) int {
	var read = 0
	for i := offset; i < length; i++ {
		if !ms.EOF() {
			ms.MemoryBuffer[ms.position] = buffer[i]
		} else {
			ms.MemoryBuffer = append(ms.MemoryBuffer, buffer[i])
		}
		ms.position++
		read++
	}
	return read
}
