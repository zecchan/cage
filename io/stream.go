package io

/*
Stream provides a generic view of a sequence of bytes.
*/
type Stream interface {
	// When overridden in a derived class, gets a value indicating whether the current stream supports reading.
	CanRead() bool
	// When overridden in a derived class, gets a value indicating whether the current stream supports writing.
	CanWrite() bool
	// When overridden in a derived class, gets a value indicating whether the current stream supports seeking.
	CanSeek() bool
	// When overridden in a derived class, gets the length in bytes of the stream.
	Length() int
	// When overridden in a derived class, gets the position within the current stream.
	Position() int
	// When overridden in a derived class, gets a value indicating whether the current stream is at the end of file.
	EOF() bool

	// Closes the current stream and releases any resources (such as sockets and file handles) associated with the current stream.
	Close()
	// Reads the bytes from the current stream and writes them to another stream. Returns the number of bytes written.
	CopyTo(stream Stream) int
	// Reads the bytes from the current stream and writes them to another stream, using a specified buffer size. Returns the number of bytes written.
	CopyBufferedTo(stream Stream, bufferSize int) int
	// When overridden in a derived class, clears all buffers for this stream and causes any buffered data to be written to the underlying device or stream.
	Flush()
	// When overridden in a derived class, reads a sequence of bytes from the current stream and advances the position within the stream by the number of bytes read. Returns the number of bytes read.
	Read(buffer []byte, offset int, length int) int
	// When overridden in a derived class, sets the position within the current stream.
	Seek(offset int, origin SeekOrigin) error
	// When overridden in a derived class, sets the length of the current stream.
	SetLength(length int)
	// When overridden in a derived class, write a sequence of bytes to the current stream and advances the position within the stream by the number of bytes written. Returns the number of bytes written.
	Write(buffer []byte, offset int, length int) int
}

// SeekOrigin is an enumerable that specify the origin point of Seek operation
type SeekOrigin int

// EnumSeekOrigin is a placeholder to hold SeekOrigin values
type EnumSeekOrigin struct {
	// Seek from the beginning of the stream
	Begin SeekOrigin
	// Seek from current position of the stream
	Current SeekOrigin
	// Seek from the end of the stream
	End SeekOrigin
}

// SeekOrigins holds values for SeekOrigin
var SeekOrigins = EnumSeekOrigin{
	Begin:   0,
	Current: 1,
	End:     2,
}
